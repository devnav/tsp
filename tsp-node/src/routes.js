// Bring in dependencies
const routes = require('express').Router();
const md5 = require('md5');
const redis = require('redis');

/* redis */
var host = process.env.REDIS_PORT_6379_TCP_ADDR || '127.0.01';
var port = process.env.REDIS_PORT_6379_TCP_PORT || 6379;
var client = redis.createClient(port, host);

const getData = require('./maps');

// Function to check if parameter is valid set of cordinates in desired format
function ensureValidLocations(destinations) {
  return new Promise((resolve, reject) => {
    if(destinations === undefined || destinations === null || !Array.isArray(destinations) || !destinations.length) {
      reject('Invalid parameters');
    } else {
      let isValid = true;
      destinations.forEach(i=> {
        isValid = isValid && Array.isArray(i) && (i.length === 2) && !isNaN(i[0]) && !isNaN(i[1])
      });
      if(isValid) resolve(destinations);
      else reject('Invalid parameters');
    }
  });
}

// Show Api documentation here
/**
 * @api {get} /route/:token Get shortest driving route
 * @apiName GetShortestRoute
 * @apiGroup Route
 *
 * @apiParam {String} token Token returned from /route api for getting the route.
 *
 * @apiSuccess {String} status success.
 * @apiSuccess {Array}  path Array of coordinated of the stop overs.
 * @apiSuccess {String} total_distance Distance of the total route.
 * @apiSuccess {String} total_time Time taken for the entire route.
 */
routes.get('/route/:token', (req, res) => {
  // Get tokem from request
  let token = req.param('token');
  if(typeof token === 'string') {
    client.get(token, function(err, value) {
      if (err) res.status(500).send(err)
      else {
        let val = JSON.parse(value);
        if(!val) {
          // Send error if Token not found
          res.status(422).json({status: 'failure', error: 'Token not found'})
        }
        else {
          // Send the path data if token is found
          res.status(200).json(val);
        }
      }
    });
  } else res.status(422).send('Invalid parameters');
});

// POST /route
/**
 * @api {post} /route Get token for retriving shortest route via api call /route/:token
 * @apiName GetTokenForShortestRoute
 * @apiGroup Route
 *
 * @apiParam {Array} body Coordinates of locations e.g. [["22.372081", "114.107877"],["22.284419", "114.159510"],["22.326442", "114.167811"]]
 *
 * @apiSuccess {String} status success.
 * @apiSuccess {String} token Token to retrieve shortest route via api /route
 */
routes.post('/route', function(req, res) {
  // Get all the destination points
  let destinations = req.body;
  ensureValidLocations(req.body).then(destinations => {
    // Sort the destination points so that even of the order in input is changed, we have the same hash
    destinations.sort((a, b) => (+a[0] - b[0]));
    // Transform to format required by google maps api
    let stringDestinations = destinations.map(i => `${i[0]}, ${i[1]}`);
    // Generate hash of destionations to avoid duplicate google maps api calls, also use this as identifier
    const token = md5(JSON.stringify(stringDestinations));
    // Check if there is already data for same set of points
    client.get(token, function(err, value) {
      if (err) res.status(500).send(err);
      else {
        let val = JSON.parse(value);
        if(!val || val.status !== 'success') {
          // If there is no previous data for same set of points
          let status = { status: "in progress" };
          // Set status in progress for a new google maps api call
          client.set(token, JSON.stringify(status), () => {
            res.json({token});
          });
          // Make the google maps api call and store the result in redis
          getData(stringDestinations).then(result => {
            client.set(token, JSON.stringify(result));
          }).catch(err => {
            res.status(500).send(err);
          });
        } else res.status(200).json({token});
      }
    });
  }).catch(err => {
    res.status(422).send(err);
  });
});

module.exports = routes;
