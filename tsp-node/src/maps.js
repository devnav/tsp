var googleMapsClient = require('@google/maps').createClient({
  key: process.env.APIKEY || 'AIzaSyBghYf-AcW0Va_ujQF9HULpohWi7j3LK58'
});

let inOneHour = Math.round((new Date().getTime() + 60 * 60 * 1000) / 1000);

function getFormattedData(geojson) {
  let response = {
    "status": null,
  };
  // Calculate for all the routes
  if(geojson.routes && geojson.routes) {
    let shortestRoute = {
      path: [],
      total_distance: null,
      total_time: null
    };
    // Find the shortest route among all the routes available
    geojson.routes.forEach(route => {
      let path = [];
      let total_distance = 0;
      let total_time = 0;

      if(route && route.legs) {
        route.legs.forEach(leg => {
          if(!isNaN(leg.distance.value)) total_distance += leg.distance.value * 1;
          if(!isNaN(leg.duration.value)) total_time += leg.duration.value * 1;
          if(leg.end_location) path.push([leg.end_location.lng, leg.end_location.lat]);
        });
      }
      console.log(total_distance, total_time)
      if(!shortestRoute.total_distance || shortestRoute.total_distance > total_distance) {
        shortestRoute = {total_time, total_distance, path};
      }
    });
    response = { ...response, ...shortestRoute , status: "success"};
  } else {
    response.status = "failure";
    response.error = "ZERO_RESULTS";
  }

  return response;
}

function getData(waypoints) {

  return new Promise((resolve, reject) => {
    if(waypoints === undefined) reject('Invalid parameters. Array expected');
    if(waypoints === null) reject('Invalid parameters. Array expected');
    if(!Array.isArray(waypoints)) waypoints = [waypoints];

    let origin = waypoints[0].split(',');
    let destination = waypoints[waypoints.length - 1].split(',');

    googleMapsClient.directions({
      origin,
      destination,
      departure_time: inOneHour,
      mode: 'driving',
      optimize: true,
      traffic_model: 'best_guess',
      waypoints
    }, function(err, response) {
      if (!err) {
        resolve(getFormattedData(response.json));
      } else reject(err);
    });

  });
}

module.exports = getData;
