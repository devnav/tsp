process.env.NODE_ENV = 'test';

var expect = require('chai').expect;
var request = require('request');

describe('API documentation', () => {
  it('Page loads', function(done) {
    request('http://localhost', function(error, response, body) {
      expect(response.statusCode, "API documentation loads successfully").to.equal(200);
      done();
    });
  });
});

describe('/POST route', () => {
  const destinations = [["22.372081", "114.107877"],["22.284419", "114.159510"],["22.326442", "114.167811"]];
  const invalidDestinations = {
    invalid: null,
    emptyString: "",
    emptyArray: [],
    invalidArray: ['a','b','c']
  };
  const baseUrl = 'http://localhost';

  it('should fail on invalid destinations', function(done) {
    let options = {
      baseUrl,
      url: '/route',
      method: 'POST',
      json: true,
      headers: {
        'Content-Type': 'application/json'
      },
      body: invalidDestinations.invalid
    }
    request(options, (err,response,body) => {
      expect(response.statusCode).to.equal(400);
      expect(body, "Body should be an error message").to.be.a('string');
      done();
    });
  });
  it('should fail on empty destinations', function(done) {
    let options = {
      baseUrl,
      url: '/route',
      method: 'POST',
      json: true,
      headers: {
        'Content-Type': 'application/json'
      },
      body: invalidDestinations.empty
    }
    request(options, (err,response,body) => {
      expect(response.statusCode).to.equal(422);
      expect(body, "Body should be an error message").to.be.a('string');
      done();
    });
  });
  it('should fail on empty array destinations', function(done) {
    let options = {
      baseUrl,
      url: '/route',
      method: 'POST',
      json: true,
      headers: {
        'Content-Type': 'application/json'
      },
      body: invalidDestinations.emptyArray
    }
    request(options, (err,response,body) => {
      expect(response.statusCode).to.equal(422);
      expect(body, "Body should be an error message").to.be.a('string');
      done();
    });
  });
  it('should fail on invalid array destinations', function(done) {
    let options = {
      baseUrl,
      url: '/route',
      method: 'POST',
      json: true,
      headers: {
        'Content-Type': 'application/json'
      },
      body: invalidDestinations.invalidArray
    }
    request(options, (err,response,body) => {
      expect(response.statusCode).to.equal(422);
      expect(body, "Body should be an error message").to.be.a('string');
      done();
    });
  });
  it('should receive token', function(done) {
    let options = {
      baseUrl,
      url: '/route',
      method: 'POST',
      json: true,
      headers: {
        'Content-Type': 'application/json'
      },
      body: destinations
    }
    request(options, (err,response,body) => {
      expect(response.statusCode).to.equal(200);
      expect(body, "Body should be an Object").to.be.a('object');
      expect(body, "Body should have token").to.have.property('token').to.be.a('string');
      done();
    });
  });
});

describe('/GET route', () => {
  const baseUrl = 'http://localhost';

  it('should receive error on invalid route', function(done) {
    let options = {
      baseUrl,
      url: '/route/',
      method: 'GET',
    }
    request(options, (err,response,body) => {
      expect(response.statusCode).to.equal(404);
      done();
    });
  });
});

describe('/GET route/:token', () => {
  const destinations = [["22.372081", "114.107877"],["22.284419", "114.159510"],["22.326442", "114.167811"]];
  const baseUrl = 'http://localhost';

  it('should receive error on invalid token', function(done) {
    let invalidToken = "83678hdbsx7e";
    let options = {
      baseUrl,
      url: `/route/${invalidToken}`,
      method: 'GET',
      json: true
    }
    request(options, (err,response,body) => {
      expect(response.statusCode).to.equal(422);
      expect(body, "Body should be an Object").to.be.a('object');
      expect(body, "Body should have token").to.have.property('status').to.be.a('string');
      expect(body, "Body should have token").to.have.property('error').to.be.a('string');
      done();
    });
  });

  it('should receive path details on valid token', function(done) {
    let options = {
      baseUrl,
      url: '/route',
      method: 'POST',
      json: true,
      headers: {
        'Content-Type': 'application/json'
      },
      body: destinations
    }
    request(options, (err,response,body) => {
      expect(response.statusCode).to.equal(200);
      expect(body, "Body should be an Object").to.be.a('object');
      expect(body, "Body should have token").to.have.property('token').to.be.a('string');

      let optionsNew = {
        baseUrl,
        url: `/route/${body.token}`,
        method: 'GET',
        json: true
      }
      request(optionsNew, (err,response,body) => {
        expect(response.statusCode).to.equal(200);
        expect(body, "Body should be an Object").to.be.a('object');
        expect(body, "Body should have status").to.have.property('status').to.be.a('string');
        // expect(body, "Body should have path").to.have.property('path').to.be.a('array');
        // expect(body, "Body should have total_distance").to.have.property('total_distance').to.be.a('number');
        // expect(body, "Body should have total_time").to.have.property('total_time').to.be.a('number');
        done();
      });
    });
  });

});
