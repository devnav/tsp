// Bring in our dependencies
const express = require('express');
const app = express();
const bodyParser = require('body-parser');

// Bring in routes
const routes = require('./src/routes');

// Configure a port number
const port = process.env.PORT || 3030;

// configure app to use bodyParser()
// this will let us get the data from a POST
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

// Connect all our routes to our application
app.use('/', routes);
// Show api doc here
app.use('/', express.static('docs'));
// Turn on that server!
app.listen(port, () => {
  console.log(`server listen port: ${port}`)
})
