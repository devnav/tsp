

# Node Api for travelling salesman problem (TSP) - using google maps api

## Docker, Node, Redis, Nginx

A node application running redis independently. Node server can be scaled horizontally depending on the load.

Nginx server acts as a load balancing for the node instances, currently there are 3 instances of node server but it can be scaled depending on load.

```mermaid
graph LR
A((Nginx)) -- Load Balance --> B(Node Server)
A -- Load Balance --> C(Node Server)
A -- Load Balance --> D(Node Server)
B --> E{Redis}
D --> E
C --> E
```

## Start
```
sudo docker-compose up
```

## API DOC
Api doc can be generated using nodejs
```
cd tsp-node
npm i
npm run apidoc
```
![Api Documentation](images/docs.jpg)

## Testing
Testing can be done using
```
cd tsp-node
npm i
npm test
```
![Testing](images/test.jpg)
